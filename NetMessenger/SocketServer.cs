﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NetMessenger
{
    public class ClientObj
    {
        public Socket Socket = null;

        public const int BufferSize = 1024;

        public byte[] Buffer = new byte[BufferSize];

        public List<string> Items = new List<string>();

        public int Sum = 0;
    }

    public class SocketServer : IDisposable
    {
        private readonly int mPort;

        private Socket MsgSocket;

        private const int MaxConnections = 10;

        private ManualResetEvent MRE = new ManualResetEvent(false);

        private List<ClientObj> ActiveClients = new List<ClientObj>();

        public SocketServer(int port)
        {
            mPort = port;
        }

        public void Start()
        {
            if (MsgSocket != null)
                return;

            try
            {
                MsgSocket = new Socket(SocketType.Stream, ProtocolType.IP);
                MsgSocket.Bind(new IPEndPoint(IPAddress.Any, mPort));

                MsgSocket.Listen(MaxConnections);

                Console.WriteLine("Wait client connection");

                while (true)
                {
                    MRE.Reset();

                    MsgSocket.BeginAccept(
                            ar =>
                            {
                                MRE.Set();

                                var listener = (Socket) ar.AsyncState;
                                var handler = listener.EndAccept(ar);
                                handler.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.KeepAlive, true);

                                Console.WriteLine("Connected client: " +
                                                  ((IPEndPoint)handler.RemoteEndPoint).Address.MapToIPv4() + ":" +
                                                  ((IPEndPoint)handler.RemoteEndPoint).Port);

                                Send(handler, "Hello dude: ");

                                var client = new ClientObj();
                                ActiveClients.Add(client);

                                client.Socket = handler;

                                handler.BeginReceive(client.Buffer, 0, ClientObj.BufferSize, 0, ReadMessage, client);

                            }, MsgSocket);

                    MRE.WaitOne();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Server crash, see error message:\n{ex.Message}");
            }
        }

        public void ReadMessage(IAsyncResult ar)
        {
            var listener = (ClientObj)ar.AsyncState;
            var handler = listener.Socket;

            var client = "[" + ((IPEndPoint)handler.RemoteEndPoint).Address.MapToIPv4() + ":" + ((IPEndPoint)handler.RemoteEndPoint).Port + "]";

            var bytesRead = handler.EndReceive(ar);
            if (bytesRead > 0)
            {
                var item = Encoding.UTF8.GetString(listener.Buffer, 0, bytesRead);
                listener.Items.Add(item);

                var text = string.Join("", listener.Items);

                if (listener.Items.Contains("\r\n"))
                {
                    //Console.WriteLine(client +
                    //                  " received all data: " +
                    //                  text.Replace("\n", ""));

                    if (text.Contains("list"))
                    {
                        foreach (var ac in ActiveClients)
                            Send(handler, "Client: [" + ((IPEndPoint)ac.Socket.RemoteEndPoint).Address.MapToIPv4() +
                                          ":" + ((IPEndPoint)ac.Socket.RemoteEndPoint).Port + "] Sum - " + ac.Sum + "\r\n");
                    }
                    else
                    {
                        int.TryParse(text, out var val);
                        listener.Sum += val;
                    }

                    listener.Items.Clear();
                }
                else if (text.Contains("\u0003"))
                {
                    Send(handler, "Bye bye!");
                    ActiveClients.Remove(listener);

                    handler.Close();
                    return;
                }
                
                // Continue read
                handler.BeginReceive(listener.Buffer, 0, ClientObj.BufferSize, 0, ReadMessage, listener);
            }
        }

        public void Send(Socket handler, string data)
        {
            byte[] byteData = Encoding.UTF8.GetBytes(data);

            handler.BeginSend(
                    byteData,
                    0,
                    byteData.Length,
                    0,
                    ar =>
                    {
                        try
                        {
                            handler.EndSend(ar);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                        }
                    }, handler);
        }

        public void Dispose()
        {
            if (MsgSocket != null)
            {
                Console.WriteLine("Close Socket connection");
                MsgSocket.Close();
            }
        }
    }

}
