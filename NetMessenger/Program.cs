﻿using System;
using System.Linq;

namespace NetMessenger
{
    class Program
    {
        static void Main(string[] args)
        {
            // Default TCP port 10 000
            int port = 10000;

            try
            {
                if (args.Length != 0)
                    int.TryParse(args.First(), out port);

                using (var socketServer = new SocketServer(port))
                {
                    socketServer.Start();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Cannot start Socket server:\n{ex.Message}");
            }

            Console.WriteLine("End program, press ENTER.");
            Console.Read();
        }
    }
}
